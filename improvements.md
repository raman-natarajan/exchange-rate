# Possible Improvements

## MongoService

- More endpoints to fetch different amount of data and metrics from the database.
- Separate database connection as a factory function or a Class for better modularity and sharing connection object.
- More accurate schema description in Swagger configuration.

## web-ui

- Breaking-up the stores (separating network calls from data storage and sharing)
- Ability to change base currency and add more currency.
- More integration with the database service (MongoService) and the store data.
