import React, { Component } from "react";
import { Provider } from "mobx-react";
import viewStore from "./stores/viewStore";
import Header from "./components/header";
import Body from "./components/body";
import _ from "lodash";

class App extends Component {
  render() {
    return (
      <Provider viewStore={viewStore}>
        <div className="App">
          <Header />
          <Body />
        </div>
      </Provider>
    );
  }
}
window._ = _;
export default App;
