import { observable, action, computed } from "mobx";
import axios from "axios";
import { eachDay, subDays, format } from "date-fns";
import config from "../config";
import _uniqBy from "lodash/uniqBy";
import _sortBy from "lodash/sortBy";
import uuid from "uuid";

class ViewStore {
  constructor() {
    this.loadData();
    this.getCurrentUSD();
  }
  @observable
  currentTime = new Date();
  @observable
  endDate = subDays(new Date(), 30);
  @observable
  latestDate = subDays(this.endDate, 30);
  @observable
  exchangeRates = [];
  @observable
  symbols = ["GBP", "EUR", "AUD", "CAD"];
  @observable
  base = "USD";
  contextId = uuid.v4();
  @observable
  currentUSDtoCAD = 1.1;

  @action
  loadData = () => {
    const endDate = this.endDate;
    const startDate = this.latestDate;
    //eslint-disable-next-line
    this.exchangeRates = [];
    eachDay(startDate, endDate).map(a => {
      const formatedDate = format(a, config.apiDateFormat);
      const url = `${
        config.exchangeRateApi
      }\\${formatedDate}?symbols=${this.symbols.join()}&base=${this.base}`;
      axios(url)
        .then(res => {
          let _data = {
            date: res.data.date,
            rates: res.data.rates
          };
          if (formatedDate !== res.data.date) {
            _data = {
              date: formatedDate,
              rates: {}
            };
          }
          this.exchangeRates.push(_data);
          // this.exchangeRates.set(res.data.date, res.data.rates);
        })
        .catch(error => {
          console.error(error);
        });
    });
    this.insertToMongo();
    this.updateLoadedTime();
  };

  @action
  getCurrentUSD = () => {
    const formatedDate = format(new Date(), config.apiDateFormat);
    const url = `${
      config.exchangeRateApi
      }\\${formatedDate}?symbols=CAD&base=${this.base}`;
    axios(url).then(res => {
      this.currentUSDtoCAD = res.data.rates.CAD;
    })
  };

  @action
  setDateRange = (range) => {
    this.latestDate = range.from;
    this.endDate = range.to;
    this.loadData();
  };

  @action
  updateLoadedTime = () => {
    this.currentTime = new Date();
  };

  @action
  insertToMongo = () => {
    const url = `${config.mongoConnection}\\insert`;
    const data = {
      contextId: this.contextId,
      body: this.uniqueExchangeData
    };
    axios
      .post(url, data)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.error(error);
      });
  };

  @action
  fetchMongoData = () => {
    const url = `${config.mongoConnection}\\fetch\\${this.contextId}`;
    axios(url).then(res => {
      console.log(res.data);
    });
  };

  @computed
  get uniqueExchangeData() {
    return _sortBy(_uniqBy(this.exchangeRates, "date"), a => new Date(a.date));
  }

  arrayToObject = (array, symbol) =>
    array.reduce((obj, item) => {
      obj[item.date] = item.rates[symbol];
      return obj;
    }, {});

  @computed
  get plottableData() {
    return [
      {
        name: "GBP",
        data: this.arrayToObject(this.uniqueExchangeData, "GBP")
      },
      {
        name: "EUR",
        data: this.arrayToObject(this.uniqueExchangeData, "EUR")
      },
      {
        name: "AUD",
        data: this.arrayToObject(this.uniqueExchangeData, "AUD")
      },
      {
        name: "CAD",
        data: this.arrayToObject(this.uniqueExchangeData, "CAD")
      },
    ];
  }
}

const viewStore = (window.viewStore = new ViewStore());
export default viewStore;
