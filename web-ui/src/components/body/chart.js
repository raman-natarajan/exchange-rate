import React from "react";
import { observer, inject } from "mobx-react";
import ReactChartkick, { LineChart } from "react-chartkick";
import HighCharts from "highcharts";

ReactChartkick.addAdapter(HighCharts);

@inject("viewStore")
@observer
class Plot extends React.Component {
  render() {
    return <LineChart data={this.props.viewStore.plottableData} />;
  }
}

export default Plot;
