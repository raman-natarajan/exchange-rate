import React from "react";
import { inject, observer } from "mobx-react";
import { CSSTransition } from "react-transition-group";
import format from "date-fns/format";
import Table from "./table";
import DatePicker from "./datePicker";
import Chart from "./chart";
import config from "../../config";

@inject("viewStore")
@observer
class Body extends React.Component {
  state = {
    showDatePicker: false
  };
  toggleDate = () => {
    this.setState({ showDatePicker: !this.state.showDatePicker });
  };
  closeDatePicker = () => {
    this.setState({ showDatePicker: false });
  };
  onClickHandler = () => {
    this.props.viewStore.loadData();
  };
  render() {
    return (
      <React.Fragment>
        <div className="button-container">
          <button
            className="button-default refresh-button"
            onClick={this.onClickHandler.bind(this)}
          >
            Refresh Data
          </button>
        </div>
        <div className="options-button-container">
          <button className="button-default" onClick={this.toggleDate}>
            <i className="fas fa-calendar-alt" title="Select date range" />
          </button>
          <p>
            {`Selected range from ${format(
              this.props.viewStore.latestDate,
              config.apiDateFormat
            )} to ${format(
              this.props.viewStore.endDate,
              config.apiDateFormat
            )}`}
          </p>
          <CSSTransition
            in={this.state.showDatePicker}
            timeout={300}
            classNames="fade"
            mountOnEnter
            unmountOnExit
          >
            <DatePicker close={this.closeDatePicker} />
          </CSSTransition>
        </div>
        <Table />
        <Chart />
      </React.Fragment>
    );
  }
}

export default Body;
