import React from "react";
import ReactTable from "react-table";
import { inject, observer } from "mobx-react";
import "react-table/react-table.css";

@inject("viewStore")
@observer
class Table extends React.Component {
  render() {
    const data = this.props.viewStore.uniqueExchangeData;
    const columns = [
      {
        Header: "Date",
        accessor: "date"
      },
      {
        Header: "USD/GBP",
        accessor: "rates.GBP"
      },
      {
        Header: "USD/EUR",
        accessor: "rates.EUR"
      },
      {
        Header: "USD/AUD",
        accessor: "rates.AUD"
      },
      {
        Header: "USD/CAD",
        accessor: "rates.CAD"
      }
    ];
    return (
      <div className="exchange-table">
        <ReactTable
          filterable
          data={data}
          columns={columns}
          defaultPageSize={10}
          className="-striped -highlight"
        />
        <br />
      </div>
    );
  }
}

export default Table;
