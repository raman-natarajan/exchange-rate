import React from "react";
import { inject, observer } from "mobx-react";
import format from "date-fns/format";

const header = inject("viewStore")(
  observer(props => {
    const date = props.viewStore.currentTime;
    return (
      <header className="header">
        <div className="rate-container">
          {`1 USD = ${props.viewStore.currentUSDtoCAD} CAD`}
          <i className="fas fa-sync-alt refresh-icon" onClick={props.viewStore.getCurrentUSD}/>
        </div>
        <div className="last-refreshed">
          {format(date, "DD/MM/YYYY H:mm:ss")}
        </div>
      </header>
    );
  })
);

export default header;
