export default {
  exchangeRateApi: "http://api.openrates.io",
  apiDateFormat: "YYYY-MM-DD",
  enableMongoApi: true,
  mongoConnection: "http://138.197.141.135:10010",
};
