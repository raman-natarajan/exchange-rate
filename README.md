# Welcome to Exchange Monitor Application!

URL to access the application: http://138.197.141.135/

**api.openrates.io does not have data for all dates, so the default period in the application was chosen around 30 days past the current date.**
Ref: https://github.com/openratesapi/openrates/issues/1


## Project Structure 

The project consists of two entities - 
MongoService - API service to communicate with the database (MongoDB) and 
web-ui - SPA (React based).

## MongoService

A very simple Swagger API hosted inside an Express Server (NodeJS 8.10.0). It posts and reads data from MongoDB (also hosted in the same server, but will work with any MongoDB instance by changing the connections string in the config file).

To run it locally, (it requires an active MongoDB connection which can be given in the config.js file)
`npm i`
`swagger project start`


## web-ui

Built using Create-React-App with MobX as state management (very good for a quick application which also has inbuilt observables feature which binds nicely with React-DOM's rerender mechanism - also provides auto refresh service to components out of the box with no extra configurations).

- Includes sort and filterable table and chart visualization.
- Feature to select a date range.

To run it locally,
`npm i`
`npm run start`