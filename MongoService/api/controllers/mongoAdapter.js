const config = require('../../config');
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const client =  new MongoClient(config.mongoDbConnection, { useNewUrlParser: true });
client.connect(err => {
    assert.equal(null, err);
    console.log("connected to server");
    const db = client.db(config.mongoDbName);
    createCapped(db);
})

function createCapped(db) {
    db.createCollection("rates", { "capped": true, "size": 100000, "max": 5000},
      function(err, results) {
        console.log("Collection created.");
      }
    );
 };

 insertData = (obj) => {
     const db = client.db(config.mongoDbName);
     db.collection("rates").insertOne(obj, (err, res) => {
         assert.equal(null, err);
         assert.equal(1, res.insertedCount);
     });
 }

 fetchData = (contextId) => {
    const db = client.db(config.mongoDbName);
    let result = {};
    db.collection("rates").findOne({contextId: contextId}, (err, res) => {
        assert.equal(null, err);
        console.log('server response', res);
    })
 }

module.exports = {
    testDb: (req, res) => {
        res.json('success');
    },
    insert: (req, res) => {
        insertData(req.swagger.params.data.value);
        result = 'success';
        console.log('here is the request', req.swagger.params.data.value);
        res.json(result);
    },
    fetch: (req, res) => {
        console.log('here is the value', req.swagger.params.contextId.value);
        const contextId = req.swagger.params.contextId.value;
        const db = client.db(config.mongoDbName);
        db.collection("rates").findOne({contextId: contextId}, (err, response) => {
            assert.equal(null, err);
            console.log('server response', response);
            res.json(response);
        })
    }
};
