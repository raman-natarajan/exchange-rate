const name = 'MongoService';
module.exports = require('rc')(name, {
    name: name,
    mongoDbConnection: 'mongodb://localhost:27017/',
    mongoDbName: 'exchangeRate',
});